// andrea diamantini - adjam7@gmail.com
// musik - simplest KDE4 audio player
// file musikui.h

#ifndef MUSIK_UI_H
#define MUSIK_UI_H

#include <phonon/seekslider.h>
#include <phonon/mediaobject.h>
#include <phonon/audiooutput.h>
#include <phonon/volumeslider.h>
#include <phonon/mediasource.h>

#include <QList>
#include <QWidget>
#include <QAction>
#include <QLineEdit>
#include <QMap>

class Musik : public QWidget
{
    Q_OBJECT
public:
    Musik();

    ~Musik();

private slots:
    void chooseFile();
    void editTag();

    void changeState(Phonon::State, Phonon::State);

private:
    void setupActions();
    void setupUi();
    void setInfo();

    QString filePath;
    QMap<QString, QString> metaData;

    Phonon::SeekSlider *seekSlider;
    Phonon::MediaObject *mediaObject;
    Phonon::AudioOutput *audioOutput;
    Phonon::VolumeSlider *volumeSlider;
    Phonon::MediaSource *source;

    QLineEdit *line;
    QAction *playAction;
    QAction *pauseAction;
    QAction *stopAction;
    QAction *editTagAction;
    QAction *openFileAction;
};

#endif
