// andrea diamantini - adjam7@gmail.com
// musik - simplest KDE4 audio player

#include <KApplication>
#include <KAboutData>
#include <KCmdLineArgs>

#include "musikclass.h"

int main (int argc, char *argv[])
{
    KAboutData aboutData( "musik", "simplest (and hopefully faster) kde4 audio player",
        ki18n("musik"), "0.0.1",
        ki18n("simplest (and hopefully faster) kde4 audio player"),
        KAboutData::License_GPL,
        ki18n("Copyright (c) 2008 Andrea Diamantini") );

    KCmdLineArgs::init( argc, argv, &aboutData );
    KApplication app;

    Musik *window = new Musik();
    window->show();

    return app.exec();
}
