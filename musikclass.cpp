// andrea diamantini - adjam7@gmail.com
// musik - simplest KDE4 audio player
// file musikui.cpp

#include <QtGui>
#include <KFileDialog>

#include "musikclass.h"
#include "musikedit.h"


Musik::Musik()
{
    audioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
    mediaObject = new Phonon::MediaObject(this);

    connect( mediaObject, SIGNAL(stateChanged(Phonon::State, Phonon::State)),
            this, SLOT(changeState(Phonon::State, Phonon::State)));

    Phonon::createPath(mediaObject, audioOutput);

    setupActions();
    setupUi();
};

Musik::~Musik()
{};


void Musik::setupActions()
{
    playAction = new QAction(style()->standardIcon(QStyle::SP_MediaPlay), 
                                                    tr("Play"), this);
    playAction->setShortcut(tr("Crl+P"));
    playAction->setDisabled(true);


    pauseAction = new QAction(style()->standardIcon(QStyle::SP_MediaPause), 
                                                    tr("Pause"), this);
    pauseAction->setShortcut(tr("Space"));
    pauseAction->setDisabled(true);


    stopAction = new QAction(style()->standardIcon(QStyle::SP_MediaStop),
                                                    tr("Stop"), this);
    stopAction->setShortcut(tr("Ctrl+S"));
    stopAction->setDisabled(true);


    openFileAction = new QAction(style()->standardIcon(QStyle::SP_FileDialogStart), 
                                                    tr("Add &Files"), this);
    openFileAction->setShortcut(tr("Ctrl+F"));


    editTagAction = new QAction(style()->standardIcon(QStyle::SP_FileDialogContentsView), 
                                                    tr("Edit Tag"), this);
    editTagAction->setShortcut(tr("Crl+E"));
    editTagAction->setDisabled(true);


    connect ( playAction,     SIGNAL( triggered() ), mediaObject, SLOT( play() ) );
    connect ( pauseAction,    SIGNAL( triggered() ), mediaObject, SLOT( pause() ) );
    connect ( stopAction,     SIGNAL( triggered() ), mediaObject, SLOT( stop() ) );
    connect ( openFileAction, SIGNAL( triggered() ), this,        SLOT( chooseFile() ) );
    connect ( editTagAction,  SIGNAL( triggered() ), this,        SLOT( editTag() ) );
}


void Musik::setupUi()
{
    line = new QLineEdit;
    line->setReadOnly(true);

    QToolBar *bar = new QToolBar;
    bar->addAction(playAction);
    bar->addAction(pauseAction);
    bar->addAction(stopAction);
    bar->addAction(openFileAction);
    bar->addAction(editTagAction);

    seekSlider = new Phonon::SeekSlider(this);
    seekSlider->setMediaObject(mediaObject);

    volumeSlider = new Phonon::VolumeSlider(this);
    volumeSlider->setAudioOutput(audioOutput);
    volumeSlider->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    QLabel *volumeLabel = new QLabel;
    volumeLabel->setPixmap(QPixmap("images/volume.png"));

    QHBoxLayout *seekerLayout = new QHBoxLayout;
    seekerLayout->addWidget(seekSlider);

    QVBoxLayout *upperLayout = new QVBoxLayout;
    upperLayout->addWidget(line);
    upperLayout->addLayout(seekerLayout);

    QHBoxLayout *playbackLayout = new QHBoxLayout;
    playbackLayout->addWidget(bar);
    playbackLayout->addStretch();
    playbackLayout->addWidget(volumeLabel);
    playbackLayout->addWidget(volumeSlider);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(upperLayout);
    mainLayout->addLayout(playbackLayout);

    setLayout(mainLayout);
    setWindowTitle("Musik");
    setWindowIcon( QIcon("/DATI/future/kde/src/musik/musik.png") );        // FIXME
}

// ----------------------------------------------------------------------

void Musik::chooseFile()
{
    filePath = KFileDialog::getOpenFileName(KUrl(), 
                                            "Audio ( *.mp3 , *.wav , *.ogg )",
                                            this,
                                            tr("Open audio file") );

    if( !filePath.isEmpty() )
    {
        mediaObject->setCurrentSource(filePath);
        playAction->setDisabled(false);
    }

    mediaObject->play();
};


void Musik::changeState(Phonon::State newState, Phonon::State /* oldState */ )
{
    switch (newState) {
        case Phonon::ErrorState:
            if (mediaObject->errorType() == Phonon::FatalError) {
                QMessageBox::warning(this, tr("Fatal Error"),
                mediaObject->errorString());
            } else {
                QMessageBox::warning(this, tr("Error"),
                mediaObject->errorString());
            }
            break;
        case Phonon::PlayingState:
                playAction->setEnabled(false);
                pauseAction->setEnabled(true);
                stopAction->setEnabled(true);
                editTagAction->setEnabled(true);
                setInfo();
                break;
        case Phonon::StoppedState:
                stopAction->setEnabled(false);
                playAction->setEnabled(true);
                pauseAction->setEnabled(false);
                break;
        case Phonon::PausedState:
                pauseAction->setEnabled(false);
                stopAction->setEnabled(true);
                playAction->setEnabled(true);
                break;
        case Phonon::BufferingState:
                break;
        default:
            ;
    }
}


void Musik::setInfo()
{
    metaData = mediaObject->metaData();

    QString info;
    QString author = metaData.value("ARTIST");
    QString title = metaData.value("TITLE");
    if (author == "" || title == "")
        info = mediaObject->currentSource().fileName();
    else
        info = author + QString(" - ") + title;

    line->setText(info);
}

void Musik::editTag()
{
    MusikEdit *win = new MusikEdit(*mediaObject);
    win->show();
};
