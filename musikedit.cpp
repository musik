// andrea diamantini - adjam7@gmail.com
// musik - simplest KDE4 audio player
// file musikedit.cpp

#include "musikedit.h"


MusikEdit::MusikEdit(Phonon::MediaObject & mo)
{
    setupUi();
    setSongInfo(mo);

    connect( okButton , SIGNAL( clicked() ), this , SLOT( ok() ) );
    connect( clearButton , SIGNAL( clicked() ), this , SLOT( clear() ) );
    connect( cancelButton , SIGNAL( clicked() ), this , SLOT( cancel() ) );
};



MusikEdit::~MusikEdit()
{};


void MusikEdit::setupUi()
{
    QLabel *artistLabel = new QLabel( tr("artist") );
    QLabel *albumLabel = new QLabel( tr("album") );
    QLabel *titleLabel = new QLabel( tr("title") );
    QLabel *dateLabel = new QLabel( tr("date") );
    QLabel *genreLabel = new QLabel( tr("genre") );
    QLabel *trackNumberLabel = new QLabel( tr("track number") );
    QLabel *descLabel = new QLabel( tr("description") );

    QVBoxLayout *labelLayout = new QVBoxLayout;
    labelLayout->addWidget(artistLabel);
    labelLayout->addWidget(albumLabel);
    labelLayout->addWidget(titleLabel);
    labelLayout->addWidget(dateLabel);
    labelLayout->addWidget(genreLabel);
    labelLayout->addWidget(trackNumberLabel);
    labelLayout->addWidget(descLabel);

    artistLine = new QLineEdit();
    albumLine = new QLineEdit();
    titleLine = new QLineEdit();
    dateLine = new QLineEdit();
    genreLine = new QLineEdit();
    trackNumberLine = new QLineEdit();
    descLine = new QLineEdit();

    QVBoxLayout *lineLayout = new QVBoxLayout;
    lineLayout->addWidget(artistLine);
    lineLayout->addWidget(albumLine);
    lineLayout->addWidget(titleLine);
    lineLayout->addWidget(dateLine);
    lineLayout->addWidget(genreLine);
    lineLayout->addWidget(trackNumberLine);
    lineLayout->addWidget(descLine);


    okButton = new QPushButton( tr("ok") );
    clearButton = new QPushButton( tr("clear") );
    cancelButton = new QPushButton( tr("cancel") );
    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(okButton);
    buttonLayout->addWidget(clearButton);
    buttonLayout->addWidget(cancelButton);

    QHBoxLayout *infoLayout = new QHBoxLayout;
    infoLayout->addLayout(labelLayout);
    infoLayout->addLayout(lineLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(infoLayout);
    mainLayout->addLayout(buttonLayout);

    setLayout(mainLayout);
    setWindowTitle("Musik Tag Editor");
};


void MusikEdit::setSongInfo(Phonon::MediaObject & mo)
{
    artistLine->setText( mo.metaData(Phonon::ArtistMetaData).at(0) );
    albumLine->setText( mo.metaData(Phonon::AlbumMetaData).at(0) );
    titleLine->setText( mo.metaData(Phonon::TitleMetaData).at(0) );
    dateLine->setText( mo.metaData(Phonon::DateMetaData).at(0) );
//    genreLine->setText( mo.metaData(Phonon::GenreMetaData).at(0) );   // FIXME phonon problem: to investigate..
    trackNumberLine->setText( mo.metaData(Phonon::TracknumberMetaData).at(0) );
    descLine->setText( mo.metaData(Phonon::DescriptionMetaData).at(0) );
};


void MusikEdit::ok()
{
    // FIXME!!
};


void MusikEdit::clear()
{
    artistLine->clear();
    albumLine->clear();
    titleLine->clear();
    dateLine->clear();
    genreLine->clear();
    trackNumberLine->clear();
    descLine->clear();
};


void MusikEdit::cancel()
{
    this->close();
};
