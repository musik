// andrea diamantini - adjam7@gmail.com
// musik - simplest KDE4 audio player
// file musikedit.h

#ifndef MUSIK_EDIT_H
#define MUSIK_EDIT_H

#include <QtGui>

#include <phonon/mediaobject.h>

class MusikEdit : public QWidget
{
    Q_OBJECT

public:
    MusikEdit(Phonon::MediaObject &);
    ~MusikEdit();

private:
    void setSongInfo(Phonon::MediaObject &);
    void setupUi();

    QLineEdit *artistLine;
    QLineEdit *albumLine;
    QLineEdit *titleLine;
    QLineEdit *dateLine;
    QLineEdit *genreLine;
    QLineEdit *trackNumberLine;
    QLineEdit *descLine;

    QPushButton *okButton;
    QPushButton *clearButton;
    QPushButton *cancelButton;

private slots:
    void ok();
    void clear();
    void cancel();

};

#endif
